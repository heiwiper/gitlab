%% Cell type:code id:5 tags:some-table

``` python
df[:2]
```

%%%% Output: execute_result

              x         y
    0  0.000000  0.000000
    1  0.256457  0.507309
